<<<<<<< HEAD
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php 
    require_once 'AdminClass.php';
    $projectName = 'client-database-creation';
    $bucketName = 'fundamental-math-1034';
    $admin = new AdminClass($projectName,$bucketName);
    
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <style>
            .h3,h3{
                text-align: center;
            }
            border-class{
                
            }
        </style>
    </head>
    <body>
        
        <?php if((count($_GET) != 0) && isset($_GET['view'])) : ?>
        <h3>Client List</h3>
        <h3>
            <a type="button" class="btn btn-default" href="?">
            <span class="glyphicon glyphicon-plus"></span> Add Client
        </a>
        </h3>
        <div class="container">
            <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                      <table id="mTable" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Position</th>
            </tr>
        </thead>
        
        <tbody>
            <?php
                echo $admin->_getListOfClient();
            ?>
        </tbody></table>
                  </div>
                  <div class="col-md-2"></div>
            </div>
        </div>
        <?php  endif; ?>
        <?php if((count($_GET) != 0) && isset($_GET['details'])) : ?>
        <h3>Client Details</h3>
        <h3>
            <a type="button" class="btn btn-default" href="?view">
            <span class="glyphicon glyphicon-search"></span> Search
        </a>
        </h3>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <?php
                    $result = $admin->_getClientDetails($_GET['details']);
                    echo $result[0];
                    ?>
                </div>
                <div class="col-md-5">
                    <!-- <iframe src="http://docs.google.com/gview?url=http://vastpost.com/sample/myFile.pdf&embedded=true" style="width:100%;height: 500px" frameborder="0"></iframe> -->
                    <h4>File Details</h4>
                    <ul>
                    <?php
                    $listOfFiles = $result[1];
                    if($listOfFiles == null){
                        echo 'No files found in client folder';
                    }else{
                        $url = 'https://storage.googleapis.com/' . $bucketName . '/'.$result[2].'/';
                        echo '<ul>';
                        foreach ($listOfFiles as $element){
                            echo '<li><a href="'.$url.$element.'" target="_blank">' .$element.'<a></li>';
                            $admin->_setPermissionPublicFile($result[2], $element);
                         }
                        echo '</ul>';
                    }
                    
                    ?>
                    </ul>
                </div>
                
            </div>
        </div>
        <?php  endif; ?>
        <?php if(count($_GET) == 0) : ?>
        <h3>Add Client</h3>
        <h3>
            <a type="button" class="btn btn-default" href="?view">
            <span class="glyphicon glyphicon-search"></span> Search
        </a>
        </h3>
        <?php
            $element = $admin->_getSingleElement();
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4" style="border: 1px solid gray; padding: 10px;margin: 5px;">
                    <form id="addClient">
                        <div id="mContainer"></div>
                        <div class="form-group">
                            <label for="id1">Client Account Name</label>
                            <input type="text" id="id1" name="param[]" placeholder="Enter Client Name here" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="id2">Client Email</label>
                            <input type="email" id="id2" name="param[]" placeholder="Enter Client Email here" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="id3">Client Company Name</label>
                            <input type="text" id="id3" name="param[]" placeholder="Enter Client Company Name here" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="id4">Client Owner Name</label>
                            <input type="text" id="id4" name="param[]" placeholder="Enter Client Owner Name here" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="id5">Client Owner Email</label>
                            <input type="email" id="id5" name="param[]" placeholder="Enter Client Owner Email here" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="param[]" class="form-control" value="<?php echo ($element == NULL)?'0':$element[0]; ?>">
                        </div>
                        <button type="button" class="btn btn-info" onclick="onClickAddClient()">Add Client</button>
                    </form>
                </div>
                <div class="col-md-7" style="border: 1px solid gray; padding: 10px;margin: 5px;">
                <?php
                
                    if($element != null){
                       if($admin->_checkForImageExtension($element[0])){
                       echo '<img src="'.$element[1].'" alt="Mountain View" style="width:100%;height:500px;">';
                    }else{
                       echo '<iframe src="http://docs.google.com/gview?url='.$element[1].'&embedded=true" style="width:100%;height:500px;" frameborder="0"></iframe>'; 
                    } 
                    }else{
                        echo '<h4>No element in images folder.</h4>';
                    }
                ?>
                </div>
                
            </div>
        </div>
        <?php  endif; ?>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#mTable').DataTable();
        });
        function onClickAddClient(){
                var postData = $('#addClient').serialize();
                postData = "addClient=&"+postData;
                console.log(postData);
                $.ajax(
                   {
                       url : "formSubmit.php",
                       type : "POST",
                       dataType : "json",
                       data : postData,
                       success:function(data)
                       {
                          //_generateMessageGeneric(data,'mContainer');
                          //console.log(data);
                           container = 'mContainer';
                          if(data[0] == '0') {
                                var msg = '<div class="alert alert-danger">'+
                                    '<strong>Warning!!! </strong>'+
                                        data[1]+
                                    '</div>';
                                $('#'+container).empty();
                                $('#'+container).append(msg);
                            }
                            else{
                                var msg = '<div class="alert alert-success">'+
                                    '<strong>Information!!! </strong>'+
                                    data[1]+
                                    '</div>';
                                $('#'+container).empty();
                                $('#'+container).append(msg);
                                window.location = '?view';
                                //window.location.reload(true);
                            }
                       }
                   });
               };
        </script>
    </body>
</html>
=======
<?php
/**
# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

# Imports the Google Cloud client library
use Google\Cloud\Storage\StorageClient;

# Your Google Cloud Platform project ID
$projectId = 'client-database-creation';

# Instantiates a client
$storage = new StorageClient([
    'projectId' => $projectId
]);

# The name for the new bucket
$bucketName = 'notional-affinity-3735';

# Creates the new bucket
$bucket = $storage->createBucket($bucketName);

echo 'Bucket ' . $bucket->name() . ' created.';
 * */
echo 'hello everybody';
>>>>>>> 6f3c31bcc74da7fa04cb0038be6276458a70531f
