<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'AdminClass.php';
$admin = new AdminClass('client-database-creation','fundamental-math-1034');
if(isset($_POST['addClient'])){
    $data = $_POST['param'];
    
    if(in_array('', $data)){
        echo json_encode(array(0,'All values must be filled up.'));
    }
    elseif (!filter_var($data[1], FILTER_VALIDATE_EMAIL) || !filter_var($data[4], FILTER_VALIDATE_EMAIL)) {
        echo json_encode(array(0,'Check Email Formats  entered.'));
    }
    elseif($admin->_checkClient($data[1])){
        echo json_encode(array(0,'Client Email already exists.'));
    }else{
        echo json_encode($admin->_addClient($data));
    };
}

