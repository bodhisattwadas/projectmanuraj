<?php

require '../vendor/autoload.php';
require_once 'connection.php';
use Google\Cloud\ServiceBuilder;
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Core\Exception\NotFoundException;

class AdminClass{
    private $gcloud;
    private $storage;
    private $bucket;
    
    function __construct($projectID,$bucketName) {
        $this->gcloud = new ServiceBuilder(['projectId' => $projectID]);
        $this->storage = $this->gcloud->storage();
        $this->bucket = $this->storage->bucket($bucketName);
    }
    
    public function _getFolderContents($folderName){
        try{
           $objects = $this->bucket->objects();
            $ar = Array();
            $length = strlen($folderName)+1;
            foreach ($objects as $object) {
                if(strpos($object->name(), $folderName) !== false){
                        $ns = substr($object->name(),$length);
                        if($ns != ''){
                            array_push($ar, $ns);
                        }
                }
            }
            return $ar;  
            
        } catch (Exception $ex) {
            return null;
        }
        
    }
    public function _getListOfClient(){
                $str = '';
                $i = 1;
                $connection = mysqli_connect(host,user,password,db);
                $q = mysqli_query($connection, "select id,client_acc_name,client_company from sample_table");
                while($xx = mysqli_fetch_row($q)){
                    $str .= '<tr>
                            <td>'.$i++.'</td>
                            <td><a href="?details='.$xx[0].'">'.$xx[1].'</a></td>
                            <td><a href="?details='.$xx[0].'">'.$xx[2].'</a></td>
                        </tr>';
                }
                return $str;
    }
    public function _getClientDetails($client){
        $connection = mysqli_connect(host,user,password,db);
        $q = mysqli_query($connection, "select * from sample_table where id = '$client'");
        $this->_createFolder($result['client_email']);
        $result = mysqli_fetch_assoc($q);
        $str =  '<h4>Client Account Name : '.$result['client_acc_name'].'<br><br>'.
                'Client Email : '.$result['client_email'].'<br><br>'.
                'Client Company Name : '.$result['client_company'].'<br><br>'.
                'Client Owner Name : '.$result['client_owner'].'<br><br>'.
                'Client Owner Email :'.$result['client_owner_email'];
        return array($str,$this->_getFolderContents($result['client_email']),$result['client_email']);
    }
    public function _checkClient($clientEmail){
        $connection = mysqli_connect(host,user,password,db);
        $q = mysqli_query($connection, "select * from sample_table where client_email = '$clientEmail'");
        return (mysqli_num_rows($q) != 0);//Return true if Client exists
    }
    public function _addClient($data){
        $connection = mysqli_connect(host,user,password,db);
        mysqli_query($connection, "insert into sample_table values ('','$data[0]','$data[1]','$data[2]','$data[3]','$data[4]')");
        if($data[5] != "0"){
            $this->_moveElement($data[5], $data[1]);
        }else{
            $this->_createFolder($data[1]);
        }
        
        return array(1,"User successfully added.");
    }
    private function _createFolder($folderName){
        $options = [
        'resumable' => false,
        'predefinedAcl' =>'publicRead', //if uploaded file needs to be shared publicly
        'name' => $folderName.'/', //provide path with folder name to create a folder. if that folder is not present, it will be created else re-used.
        'metadata' => [
            'contentLanguage' => 'en'
        ]
        ];
        $this->bucket->upload(fopen('command.txt', 'r'),$options);
    }
    public function _getSingleElement(){
        $ar = $this->_getFolderContents('images');
        if($ar != NULL){
            shuffle($ar);
            $element = array_pop($ar);//Obtained Element
            $this->_setPermissionPublicFile('images', $element);
            $elementURL = 'https://storage.googleapis.com/fundamental-math-1034/images/'.$element;//Obtained Element URL
            return array($element,$elementURL); 
        }else{
            return NULL;
        }
        
    }
    public function _moveElement($fileName,$destFolder){
        $options = ['name' => $destFolder.'/'.$fileName];
        $object = $this->bucket->object('images/'.$fileName);
        $otherBucket = $this->storage->bucket('fundamental-math-1034');
        $this->_createFolder($destFolder);
        $object->copy($otherBucket, $options);
        $object->delete();
    }
    public function _setPermissionPublicALLElements($folderName){
        $array = $this->_getFolderContents($folderName);
        foreach($array as $element){
            $options = ['predefinedAcl' =>'publicRead'];
            $object = $this->bucket->object($folderName.'/'.$element);
            $object->update([],$options);
        }
    }
    public function _setPermissionPublicFile($folderName,$fileName){
        $array = $this->_getFolderContents($folderName);
        $options = ['predefinedAcl' =>'publicRead'];
        $object = $this->bucket->object($folderName.'/'.$fileName);
        $object->update([],$options);
        }
    public function _checkForImageExtension($fileName){
        $r = explode('.', strtolower($fileName));
        $len = sizeof($r);
        $extension = $r[$len-1];
        return in_array($extension, array('jpeg','jpg','png','gif','bmp'));
    }
    public function _print($min,$max){
        $connection = mysqli_connect(host,user,password,db);
        $q = mysqli_query($connection, "select client_email from sample_table where id between '$min' and '$max'");
        while($xx = mysqli_fetch_row($q)){
            $this->_createFolder($xx[0]);
        }
    }
}
